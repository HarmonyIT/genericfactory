import Object_Factory
import SpotifyService
import PandoraService
import LocalService
import music


class MusicServiceProvider(Object_Factory.ObjectFactory):
    def get(self, service_id, **kwargs):
        return self.create(service_id, **kwargs)


services = music.MusicServiceProvider()
services.register_builder('SPOTIFY', SpotifyService.SpotifyServiceBuilder())
services.register_builder('PANDORA', PandoraService.PandoraServiceBuilder())
services.register_builder('LOCAL', LocalService.create_local_music_service)

